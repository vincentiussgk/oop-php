<?php
	class animal{
		public $name,
		$legs = 2,
		$cold_blooded = False;
		public function __construct($name){
			$this->name = $name;
		}
		public function get_name(){
			return $this->name;
		}
		public function get_legs(){
			return $this->legs;
		}
		public function get_cold_blooded(){
			return $this->cold_blooded;
		}
	} 
	$sheep = new animal ("shaun");
	echo $sheep->get_name($sheep) . "<br>";
	echo $sheep->get_legs($sheep) . "<br>";
	echo $sheep->get_cold_blooded($sheep) . "<br>";
?>