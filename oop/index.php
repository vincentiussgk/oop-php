<?php
	require_once ("animal.php");
	require_once ("ape.php");
	require_once ("frog.php");
	$sungokong = new ape("kera sakti");
	$sungokong->yell();
	echo $sungokong->get_name($sungokong) . "<br>";
	echo $sungokong->get_legs($sungokong) . "<br>";
	echo $sungokong->get_cold_blooded($sungokong) . "<br>";

	$kodok = new frog("buduk");
	$kodok->jump();
	echo $kodok->get_name($kodok) . "<br>";
	echo $kodok->get_legs($kodok) . "<br>";
	echo $kodok->get_cold_blooded($kodok) . "<br>";
?>